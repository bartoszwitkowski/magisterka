(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     27045,        759]
NotebookOptionsPosition[     26757,        745]
NotebookOutlinePosition[     27102,        760]
CellTagsIndexPosition[     27059,        757]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"parametry", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"width", "->", "0.25"}], ",", "\[IndentingNewLine]", 
     RowBox[{"length", "->", "0.35"}], ",", "\[IndentingNewLine]", 
     RowBox[{"height", "->", "0.07"}], ",", "\[IndentingNewLine]", 
     RowBox[{"Mk", "\[Rule]", "0.025"}], ",", "\[IndentingNewLine]", 
     RowBox[{"wheelradius", "->", "0.1"}], ",", "\[IndentingNewLine]", 
     RowBox[{"wheelwidth", "->", "0.014"}], ",", "\[IndentingNewLine]", 
     RowBox[{"Mp", "->", "3.2"}], ",", "\[IndentingNewLine]", 
     "\[IndentingNewLine]", 
     RowBox[{"dx13", "\[Rule]", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"dx1", "+", "dx3"}], ")"}], "/", "2"}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"dy13", "\[Rule]", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"dy1", "+", "dy3"}], ")"}], "/", "2"}]}], ",", 
     "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"dx1", " ", "->", " ", "0.15"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy1", " ", "->", " ", "0.1"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dx2", " ", "->", " ", "0"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy2", " ", "->", " ", "0.15"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dx3", " ", "->", " ", 
      RowBox[{"-", "0.15"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy3", " ", "->", " ", "0.1"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dx4", " ", "->", " ", "0.15"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy4", " ", "->", " ", 
      RowBox[{"-", "0.1"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dx5", " ", "->", " ", "0"}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy5", " ", "->", " ", 
      RowBox[{"-", "0.1"}]}], ",", 
     RowBox[{"(*", 
      RowBox[{
       RowBox[{"Ma", " ", "byc", " ", "0.15"}], ",", " ", 
       RowBox[{"jest", " ", "0.1", " ", "dla", " ", "symetrycznosci"}]}], 
      "*)"}], "\[IndentingNewLine]", 
     RowBox[{"dx6", " ", "->", " ", 
      RowBox[{"-", "0.15"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"dy6", " ", "->", " ", 
      RowBox[{"-", "0.1"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"R", "\[Rule]", "wheelradius"}], ",", "\[IndentingNewLine]", 
     RowBox[{"f1", "\[Rule]", 
      RowBox[{"0.001", "*", "Frictionenabled"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"f2", "\[Rule]", 
      RowBox[{"0.001", "*", "Frictionenabled"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"f3", "\[Rule]", 
      RowBox[{"0.001", "*", "Frictionenabled"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"f4", "\[Rule]", 
      RowBox[{"0.005", "*", "Frictionenabled"}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"f5", "\[Rule]", 
      RowBox[{"0.005", "*", "Frictionenabled"}]}], ",", "\[IndentingNewLine]",
      "\[IndentingNewLine]", 
     RowBox[{"Frictionenabled", "\[Rule]", "1"}], ",", "\[IndentingNewLine]", 
     RowBox[{"g", "\[Rule]", "9.81"}]}], "\[IndentingNewLine]", "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"momenty", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"Iz", "->", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "/", "12"}], ")"}], "Mp", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"length", "^", "2"}], "+", 
         RowBox[{"width", "^", "2"}]}], ")"}]}]}], ",", "\[IndentingNewLine]", 
     RowBox[{"Izz", "->", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "/", "12"}], ")"}], "Mk", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"3", 
          RowBox[{"wheelradius", "^", "2"}]}], "+", 
         RowBox[{"wheelwidth", "^", "2"}]}], ")"}]}]}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Ixx", "->", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "/", "2"}], ")"}], "Mk", " ", "*", 
       RowBox[{"wheelradius", "^", "2"}]}]}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.709870674313349*^9, 3.709870814368086*^9}, {
  3.7098709308262224`*^9, 3.709870947160694*^9}, {3.7098710056084623`*^9, 
  3.7098710274550967`*^9}, {3.7098710883100653`*^9, 3.70987113016675*^9}, {
  3.7098711957691016`*^9, 3.70987119720027*^9}, {3.709871234150341*^9, 
  3.709871256616679*^9}, {3.7098713714834456`*^9, 3.7098714198361216`*^9}, {
  3.7098714675200033`*^9, 3.7098715339894233`*^9}, {3.709871597050722*^9, 
  3.709871603617211*^9}, {3.70987164359466*^9, 3.709871674636464*^9}, {
  3.7098717299491415`*^9, 3.709871749473458*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"q", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"x", "[", "t", "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"y", "[", "t", "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"phi", "[", "t", "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"theta13", "[", "t", "]"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"theta5", "[", "t", "]"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"P", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          FractionBox["1", "2"], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"3", " ", "Mk"}], "+", "Mp"}], ")"}]}], ",", "0", ",", 
         RowBox[{
          RowBox[{"-", "Mk"}], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dy1", "+", "dy3", "+", "dy5"}], ")"}], " ", 
             RowBox[{"Cos", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}], "+", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dx1", "+", "dx3", "+", "dx5"}], ")"}], " ", 
             RowBox[{"Sin", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}]}], ")"}]}], ",", "0", 
         ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", 
         RowBox[{
          FractionBox["1", "2"], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"3", " ", "Mk"}], "+", "Mp"}], ")"}]}], ",", 
         RowBox[{"Mk", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dx1", "+", "dx3", "+", "dx5"}], ")"}], " ", 
             RowBox[{"Cos", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}], "-", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dy1", "+", "dy3", "+", "dy5"}], ")"}], " ", 
             RowBox[{"Sin", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}]}], ")"}]}], ",", "0", 
         ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "Mk"}], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dy1", "+", "dy3", "+", "dy5"}], ")"}], " ", 
             RowBox[{"Cos", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}], "+", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dx1", "+", "dx3", "+", "dx5"}], ")"}], " ", 
             RowBox[{"Sin", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}]}], ")"}]}], ",", 
         RowBox[{"Mk", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dx1", "+", "dx3", "+", "dx5"}], ")"}], " ", 
             RowBox[{"Cos", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}], "-", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{"dy1", "+", "dy3", "+", "dy5"}], ")"}], " ", 
             RowBox[{"Sin", "[", 
              RowBox[{"phi", "[", "t", "]"}], "]"}]}]}], ")"}]}], ",", 
         RowBox[{
          FractionBox["1", "2"], " ", 
          RowBox[{"(", 
           RowBox[{"Iz", "+", 
            RowBox[{"3", " ", "Izz"}], "+", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{
               SuperscriptBox["dx1", "2"], "+", 
               SuperscriptBox["dx3", "2"], "+", 
               SuperscriptBox["dx5", "2"], "+", 
               SuperscriptBox["dy1", "2"], "+", 
               SuperscriptBox["dy3", "2"], "+", 
               SuperscriptBox["dy5", "2"]}], ")"}], " ", "Mk"}]}], ")"}]}], 
         ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0", ",", "Ixx", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0", ",", "0", ",", 
         FractionBox["Ixx", "2"]}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Cc", "[", 
      RowBox[{
      "P_", ",", "q_", ",", " ", "i_", ",", " ", "j_", ",", " ", "k_"}], 
      "]"}], ":=", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "/", "2"}], ")"}], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"P", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "j"}], "]"}], "]"}], ",", 
          RowBox[{"q", "[", 
           RowBox[{"[", "k", "]"}], "]"}]}], "]"}], "+", " ", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"P", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "k"}], "]"}], "]"}], ",", 
          RowBox[{"q", "[", 
           RowBox[{"[", "j", "]"}], "]"}]}], "]"}], "-", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"P", "[", 
           RowBox[{"[", 
            RowBox[{"j", ",", "k"}], "]"}], "]"}], ",", 
          RowBox[{"q", "[", 
           RowBox[{"[", "i", "]"}], "]"}]}], "]"}]}], ")"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Ccksum", "[", 
      RowBox[{"P_", ",", "q_", ",", "i_", ",", "j_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"Cc", "[", 
        RowBox[{"P", ",", "q", ",", "i", ",", "j", ",", "1"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"q", "[", 
          RowBox[{"[", "1", "]"}], "]"}], ",", "t"}], "]"}]}], "+", 
      RowBox[{
       RowBox[{"Cc", "[", 
        RowBox[{"P", ",", "q", ",", "i", ",", "j", ",", "2"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"q", "[", 
          RowBox[{"[", "2", "]"}], "]"}], ",", "t"}], "]"}]}], "+", 
      RowBox[{
       RowBox[{"Cc", "[", 
        RowBox[{"P", ",", "q", ",", "i", ",", "j", ",", "3"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"q", "[", 
          RowBox[{"[", "3", "]"}], "]"}], ",", "t"}], "]"}]}], "+", 
      RowBox[{
       RowBox[{"Cc", "[", 
        RowBox[{"P", ",", "q", ",", "i", ",", "j", ",", "4"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"q", "[", 
          RowBox[{"[", "4", "]"}], "]"}], ",", "t"}], "]"}]}], "+", 
      RowBox[{
       RowBox[{"Cc", "[", 
        RowBox[{"P", ",", "q", ",", "i", ",", "j", ",", "5"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"q", "[", 
          RowBox[{"[", "5", "]"}], "]"}], ",", "t"}], "]"}]}]}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C11", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "1", ",", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C12", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "1", ",", "2"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C13", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "1", ",", "3"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C14", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "1", ",", "4"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C15", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "1", ",", "5"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C21", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "2", ",", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C22", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "2", ",", "2"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C23", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "2", ",", "3"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C24", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "2", ",", "4"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C25", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "2", ",", "5"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C31", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "3", ",", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C32", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "3", ",", "2"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C33", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "3", ",", "3"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C34", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "3", ",", "4"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C35", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "3", ",", "5"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C41", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "4", ",", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C42", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "4", ",", "2"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C43", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "4", ",", "3"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C44", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "4", ",", "4"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C45", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "4", ",", "5"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C51", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "5", ",", "1"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C52", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "5", ",", "2"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C53", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "5", ",", "3"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C54", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "5", ",", "4"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"C55", "=", 
     RowBox[{"Ccksum", "[", 
      RowBox[{"P", ",", "q", ",", " ", "5", ",", "5"}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"MD", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
        "C11", ",", " ", "C12", ",", " ", "C13", ",", " ", "C14", ",", " ", 
         "C15"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
        "C21", ",", " ", "C22", ",", " ", "C23", ",", " ", "C24", ",", " ", 
         "C25"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
        "C31", ",", " ", "C32", ",", " ", "C33", ",", " ", "C34", ",", " ", 
         "C35"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
        "C41", ",", " ", "C42", ",", " ", "C43", ",", " ", "C44", ",", " ", 
         "C45"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
        "C51", ",", " ", "C52", ",", " ", "C53", ",", " ", "C54", ",", " ", 
         "C55"}], "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"B", " ", "=", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"1", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "1"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Ha", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", 
         RowBox[{"-", "dy13"}], ",", " ", 
         RowBox[{"-", "R"}], ",", " ", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", 
         RowBox[{"-", "dy5"}], ",", "0", ",", 
         RowBox[{"-", "R"}]}], "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Haa", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", 
          RowBox[{"Sin", "[", 
           RowBox[{"phi", "[", "t", "]"}], "]"}]}], ",", 
         RowBox[{"Cos", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", " ", "dx13", ",", " ", 
         "0", ",", "0"}], " ", "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", 
          RowBox[{"Sin", "[", 
           RowBox[{"phi", "[", "t", "]"}], "]"}]}], ",", 
         RowBox[{"Cos", "[", 
          RowBox[{"phi", "[", "t", "]"}], "]"}], ",", " ", "dx5", ",", " ", 
         "0", ",", "0"}], " ", "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"G", "=", 
     RowBox[{
      RowBox[{"Transpose", "[", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"Sin", "[", 
            RowBox[{"phi", "[", "t", "]"}], "]"}], ",", 
           RowBox[{"-", 
            RowBox[{"Cos", "[", 
             RowBox[{"phi", "[", "t", "]"}], "]"}]}], ",", " ", "0", ",", " ",
            "0", ",", " ", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
          "0", ",", " ", "0", ",", " ", "R", ",", " ", "dy13", ",", " ", 
           "dy5"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"R", "*", 
            RowBox[{"Cos", "[", 
             RowBox[{"phi", "[", "t", "]"}], "]"}]}], ",", " ", 
           RowBox[{"R", "*", 
            RowBox[{"Sin", "[", 
             RowBox[{"phi", "[", "t", "]"}], "]"}]}], ",", "0", ",", " ", "1",
            ",", "1"}], "}"}]}], "}"}], "]"}], "//", "Simplify"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"N1", " ", "=", " ", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"0.5", "*", "Mp"}], "+", 
        RowBox[{"3", "Mk"}]}], ")"}], "*", "g"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"N2", " ", "=", " ", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"0.5", "*", "Mp"}], "+", 
        RowBox[{"3", "Mk"}]}], ")"}], "*", "g"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Epsilon]1", "=", "0.01"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Epsilon]2", "=", "0.01"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"g", "=", "9.81"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Beta]1", "=", 
     RowBox[{"\[Epsilon]1", "*", "N1"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Beta]2", "=", 
     RowBox[{"\[Epsilon]1", "*", "N2"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"\[Beta]a", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"\[Beta]1", ",", " ", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "\[Beta]2"}], "}"}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"r\[Alpha]", "=", 
     RowBox[{"-", 
      RowBox[{"\[Beta]a", ".", "Haa", ".", 
       RowBox[{"D", "[", 
        RowBox[{"q", ",", " ", "t"}], "]"}]}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Ff", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "f1"}], "*", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"x", "[", "t", "]"}], ",", "t"}], "]"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "f2"}], "*", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"y", "[", "t", "]"}], ",", "t"}], "]"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "f3"}], "*", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"phi", "[", "t", "]"}], ",", "t"}], "]"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "f4"}], "*", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"theta13", "[", "t", "]"}], ",", "t"}], "]"}]}], "}"}], 
       ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "f5"}], "*", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"theta5", "[", "t", "]"}], ",", "t"}], "]"}]}], "}"}]}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Fs", "=", 
     RowBox[{
      RowBox[{"Transpose", "[", "Haa", "]"}], ".", "r\[Alpha]"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{
     RowBox[{"n", "=", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"n1", "[", "t", "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"n2", "[", "t", "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"n3", "[", "t", "]"}], "}"}]}], "}"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"u", "=", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"u1", "[", "t", "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"u2", "[", "t", "]"}], "}"}]}], "}"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"k", "=", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "[", "t", "]"}], ",", 
        RowBox[{"y", "[", "t", "]"}], ",", 
        RowBox[{"phi", "[", "t", "]"}]}], "}"}]}], ";", "\[IndentingNewLine]",
      "\[IndentingNewLine]", 
     RowBox[{"x", "=", 
      RowBox[{"Transpose", "[", 
       RowBox[{"Join", "[", 
        RowBox[{"q", ",", "n"}], "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"xp", "=", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"x", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"y", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"phi", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", " ", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"theta13", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", 
        " ", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"theta5", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"n1", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"n2", "[", "t", "]"}], ",", "t"}], "]"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"n3", "[", "t", "]"}], ",", "t"}], "]"}], "}"}]}], "}"}]}],
      ";", "\[IndentingNewLine]", 
     RowBox[{"ok", "=", 
      RowBox[{"2", "*", 
       RowBox[{"Pi", "/", "TT"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Ps", "=", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"1", ",", 
          RowBox[{"Sin", "[", 
           RowBox[{"ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Cos", "[", 
           RowBox[{"ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Sin", "[", 
           RowBox[{"2", "*", "ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Cos", "[", 
           RowBox[{"2", "*", "ok", "*", "t"}], "]"}], ",", "0", ",", "0", ",",
           "0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0", ",", "0", ",", "0", ",", "1", ",", 
          RowBox[{"Sin", "[", 
           RowBox[{"ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Cos", "[", 
           RowBox[{"ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Sin", "[", 
           RowBox[{"2", "*", "ok", "*", "t"}], "]"}], ",", 
          RowBox[{"Cos", "[", 
           RowBox[{"2", "*", "ok", "*", "t"}], "]"}]}], "}"}]}], "}"}]}], ";",
      "\[IndentingNewLine]", 
     RowBox[{"X", "=", 
      RowBox[{
       RowBox[{"Inverse", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", "G\[Alpha]", "]"}], ".", "P", ".", 
         "G\[Alpha]"}], "]"}], ".", 
       RowBox[{"Transpose", "[", "G\[Alpha]", "]"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Y", "=", 
      RowBox[{
       RowBox[{"P", ".", 
        RowBox[{"D", "[", 
         RowBox[{"G\[Alpha]", ",", "t"}], "]"}], ".", "n"}], "-", "MD", "+", 
       RowBox[{
        RowBox[{"Transpose", "[", "Ha", "]"}], ".", "r\[Alpha]"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"f", " ", "=", " ", 
      RowBox[{"Join", "[", 
       RowBox[{
        RowBox[{"G\[Alpha]", ".", "n"}], ",", 
        RowBox[{"X", ".", "Y"}]}], "]"}]}], ";"}]}]}]}]], "Input",
 CellChangeTimes->{{3.7098717574197025`*^9, 3.7098719082424817`*^9}, {
  3.7098719481225195`*^9, 3.709871973695086*^9}, {3.7098720315269747`*^9, 
  3.7098720331204076`*^9}, {3.7098720735832853`*^9, 3.7098721283081684`*^9}}]
},
WindowSize->{958, 930},
WindowMargins->{{-7, Automatic}, {-7, Automatic}},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (November 20, 2012)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 4409, 95, 752, "Input"],
Cell[4969, 117, 21784, 626, 1588, "Input"]
}
]
*)

(* End of internal cache information *)
